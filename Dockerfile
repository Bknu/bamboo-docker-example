FROM node:latest

RUN mkdir /src

WORKDIR /src

COPY . /src
RUN npm install

EXPOSE 3000

CMD ["node", "/src/server.js"]
